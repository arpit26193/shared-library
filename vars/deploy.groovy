def call(Map deploy){
    sh "aws s3 cp s3://${deploy.Key_File_Path_S3}/key ${deploy.WORKSPACE}/devops/ansible/key"
    sh "aws s3 cp s3://${deploy.newrelic_File_Path_S3}/newrelic-java-5.6.0.zip ${deploy.WORKSPACE}/devops/ansible/newrelic-java-5.6.0.zip"
    sh "chmod 600 ${deploy.WORKSPACE}/devops/ansible/key"
    sh "ansible-playbook -i ${deploy.WORKSPACE}/devops/ansible/inventory/ ${deploy.WORKSPACE}/devops/ansible/java.yml --private-key=${deploy.WORKSPACE}/devops/ansible/key -u ubuntu -e app_name=${deploy.app_name} -e env=${deploy.env} -e WORKSPACE=${deploy.WORKSPACE}"
}