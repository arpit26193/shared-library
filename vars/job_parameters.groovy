def call(){
    parameters{
        choice(
            name: 'appENV',
            choices: "dev\nstage",
            description: 'Choose your ENV' )
        string(
            name: 'appBRANCH',  
            defaultValue: "development",
            description: 'Write down your Tag/Branch to be deployed' )
        gitParameter( 
            name: 'BRANCH_TAG',
            type: 'PT_BRANCH_TAG',
            defaultValue: 'master')
    }
}