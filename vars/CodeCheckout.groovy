def call(Map stageParams){
    checkout([$class: 'GitSCM', 
                branches: [[name: stageParams.appBRANCH]], 
                userRemoteConfigs: [[ url: stageParams.gitUrl ]]
            ])
}